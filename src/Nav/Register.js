// Render Prop
import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import "bootstrap/dist/css/bootstrap.min.css";

const Register = () => (
  <div>
    <Formik
      initialValues={{ name: "", email: "", password: "", repassword: "" }}
      validate={values => {
        const errors = {};
        if (!values.name) {
          errors.name = "Name Required";
        } else if (!values.email) {
          errors.email = "Email Required";
        } else if (
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
          errors.email = "Invalid email address";
        } else if (!values.password) {
          errors.password = "Password Required";
        } else if (
          !/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/i.test(values.password)
        ) {
          errors.password =
            "Password Minimum eight characters, at least one letter and one number:";
        } else if (values.password !== values.repassword) {
          errors.password = "Confirm Password must be same with Password";
        }
        return errors;
      }}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          alert(JSON.stringify(values, null, 2));
          setSubmitting(false);
        }, 400);
      }}
    >
      {({ isSubmitting }) => (
        <div className="App-header">
        <Form>
          <div class="form-group " >
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1">
                @
              </span>
              <Field
                placeholder="Name"
                type=""
                name="name"
                class="form-control"
              />
              <br />
            </div>
            <Field
              placeholder="Email"
              type="email"
              name="email"
              class="form-control"
            />{" "}
            <br />
            <Field
              placeholder="Password"
              type="password"
              name="password"
              class="form-control"
            />
            <br />
            <Field
              placeholder="Confirm Password"
              type="password"
              name="repassword"
              class="form-control"
            />{" "}
            <br />
            <button
              type="submit"
              class="btn btn-primary"
              disabled={isSubmitting}
            >
              Submit
            </button>
            <ErrorMessage
              name="name"
              component="div"
              class="alert alert-danger"
              role="alert"
            />
            <ErrorMessage
              name="email"
              component="div"
              class="alert alert-danger"
              role="alert"
            />
            <ErrorMessage
              name="password"
              component="div"
              class="alert alert-danger"
              role="alert"
            />
            <ErrorMessage
              name="repassword"
              component="div"
              class="alert alert-danger"
              role="alert"
           />
          
          </div> 
          
                  
        </Form>
        
        </div>
      )}
    </Formik>
  </div>
);

export default Register;
