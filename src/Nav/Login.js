// Render Prop
import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import "bootstrap/dist/css/bootstrap.min.css";

const Login = () => (
  <div>
    <Formik
      initialValues={{ email: "", password: "",}}
      validate={values => {
        const errors = {};
        if (!values.email) {
          errors.email = "Name Required";
        } else if (!values.password) {
          errors.password = "Email Required";
        }
        return errors;
      }}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          alert(JSON.stringify(values, null, 2));
          setSubmitting(false);
        }, 400);
      }}
    >
      {({ isSubmitting }) => (
        <div className="App-header">
        <Form>
        <img src="https://2.bp.blogspot.com/-2TZJ3M9sSx4/Vs7gZnFNwgI/AAAAAAAABUM/v3R88LMR0Zc/s1600/login.png"/>
          <div class="form-group " >
            <div class="input-group-prepend">
            </div>
            <Field
              placeholder="Email"
              type="email"
              name="email"
              class="form-control"
            />{" "}
            <br />
            <Field
              placeholder="Password"
              type="password"
              name="password"
              class="form-control"
            />
            <br />
            <button
              type="submit"
              class="btn btn-primary"
              disabled={isSubmitting}
            >
              Submit
            </button>
            <ErrorMessage
              name="email"
              component="div"
              class="alert alert-danger"
              role="alert"
            />
            <ErrorMessage
              name="password"
              component="div"
              class="alert alert-danger"
              role="alert"
            />
          
          </div> 
          
                  
        </Form>
        
        </div>
      )}
    </Formik>
  </div>
);

export default Login;
